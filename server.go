package main

import (
    //   "log"
    "database/sql"
    "fmt"
    "github.com/gin-gonic/gin"
    _ "github.com/mattn/go-sqlite3"
    "log"
    "regexp"
    "strconv"
)

type Connection struct {
    Id int              `json:"id"`
    IpSrc string        `json:"ip_src"`
    PSrc int            `json:"p_src"`
    IpDest string       `json:"ip_dest"`
    PDest int           `json:"p_dest"`
    Packets []Packet    `json:"packets"`
}

type Packet struct {
    Id int              `json:"id"`
    Src string          `json:"src"`
    Timestamp float64   `json:"timestamp"`
    Content []byte      `json:"content"`
}

type SearchResponse struct {
    Start int                   `json:"start"`
    End int                     `json:"end"`
    Connections []Connection    `json:"connections"`
}

var DB_PATH = ""
var SERVICES = make([]Service, 0)

func navigate(c *gin.Context) {
    database, _ := sql.Open("sqlite3", DB_PATH)
    defer database.Close()
    tx, _ := database.Begin()

    limit, _ := strconv.Atoi(c.DefaultQuery("limit", "100"))
    offset, _ := strconv.Atoi(c.DefaultQuery("offset", "0"))
    service, _ := strconv.Atoi(c.DefaultQuery("service", "-1"))

    tableName := "connections"
    filter := c.Query("filter")
    if filter != "" && filter != "-1" {
        reg, err := regexp.Compile("[^0-9]+")
        if err != nil {
            log.Println(err)
            return
        }
        cleanedFilter := reg.ReplaceAllString(filter, "")
        tableName = fmt.Sprintf("result_%s", cleanedFilter)
    }

    portCondition := ""
    if service > 0 {
        portCondition = fmt.Sprintf(" WHERE (p_src = %d OR p_dest = %d)", service, service)
    }

    query := fmt.Sprintf(`SELECT id, ip_src, p_src, ip_dest, p_dest, timestamp, content, connID FROM connections where connID IN 
    (SELECT DISTINCT connID from %s %s limit ? offset ?)`, tableName, portCondition)
    fmt.Println(query)

    rows, _ := tx.Query(query, limit, offset)

	var connections = make([]Connection, 0)

	var connectionId int = 1
    var ipSrc string
    var pSrc int
    var ipDest string
    var pDest int
    var timestamp float64
    var packetId int
    var content []byte

	var lastConnectionId = 1
    var lastConnection Connection
    var isFirst = true

	for rows.Next() {
	    rows.Scan(&packetId, &ipSrc, &pSrc, &ipDest, &pDest, &timestamp, &content, &connectionId)

        if isFirst {
            lastConnectionId = connectionId
        }

        if connectionId != lastConnectionId {
             connections = append(connections, lastConnection)
             lastConnection = Connection{}
             lastConnectionId = connectionId
        }

        packet := Packet{Id: packetId, Timestamp: timestamp, Content: content, Src: ipSrc}
        lastConnection.Id = connectionId
        lastConnection.IpSrc = ipSrc
        lastConnection.PSrc = pSrc
        lastConnection.IpDest = ipDest
        lastConnection.PDest = pDest
        lastConnection.Packets = append(lastConnection.Packets, packet)

        isFirst = false
    }

    // Add the last connection
    if (!isFirst) {
        connections = append(connections, lastConnection)
    }

    searchResponse := SearchResponse{Start: offset, End: (offset+limit), Connections: connections}

	c.JSON(200, searchResponse)
}

type Statistics struct {
    ConnectionNumber int    `json:"connection_number"`
}

func statistics(c *gin.Context) {
    database, _ := sql.Open("sqlite3", DB_PATH)
    defer database.Close()

    tx, _ := database.Begin()

    tableName := "connections"
    filter := c.Query("filter")
    if filter != "" && filter != "-1" {
        reg, err := regexp.Compile("[^0-9]+")
        if err != nil {
            log.Println(err)
            return
        }
        cleanedFilter := reg.ReplaceAllString(filter, "")
        tableName = fmt.Sprintf("result_%s", cleanedFilter)
    }

    rows, _ := tx.Query(fmt.Sprintf(`select count(distinct connID) from %s`, tableName))

	var connectionNumber = 0

	for rows.Next() {
	    rows.Scan(&connectionNumber)
    }
    
    statistics := Statistics{ConnectionNumber: connectionNumber}

	c.JSON(200, statistics)
}

type Filter struct {
    Id int              `json:"id"`
    Regex string        `json:"regex"`
    Results int         `json:"results"`
    Time float64   `json:"time"`
}

func filters(c *gin.Context) {
    database, _ := sql.Open("sqlite3", DB_PATH)
    defer database.Close()
    tx, _ := database.Begin()

    rows, _ := tx.Query(`SELECT id, regex, results, time FROM filters`)

    var filters = make([]Filter, 0)

    var id int
    var regex string
    var results int
    var ctime float64

    for rows.Next() {
        rows.Scan(&id, &regex, &results, &ctime)

        filters = append(filters, Filter{Id: id, Regex:regex, Results:results, Time:ctime})
    }

    c.JSON(200, filters)
}

func filterPackets(c *gin.Context) {
    filter := c.Query("filter")
    reg, err := regexp.Compile("[^0-9]+")
    if err != nil {
        log.Println(err)
        return
    }
    cleanedFilter := reg.ReplaceAllString(filter, "")

    database, _ := sql.Open("sqlite3", DB_PATH)
    defer database.Close()
    tx, _ := database.Begin()

    rows, _ := tx.Query(fmt.Sprintf(`SELECT id, connID FROM result_%s`, cleanedFilter))

    var output = make(map[int][]int)

    var id int
    var connID int

    for rows.Next() {
        rows.Scan(&id, &connID)

        output[connID] = append(output[connID], id)
    }

    c.JSON(200, output)
}

func services(c *gin.Context) {
    c.JSON(200, SERVICES)
}

func StartServer(config Config) {
    DB_PATH = config.DBPath
    SERVICES = config.Services

    router := gin.Default()
    router.GET("/navigate", navigate)
    router.GET("/statistics", statistics)
    router.GET("/filters", filters)
    router.GET("/services", services)
    router.GET("/filter_packets", filterPackets)
    router.LoadHTMLGlob("build/*.html")    // load the built dist path
    router.LoadHTMLFiles("build/static/*/*") //  load the static path
    router.Static("/static", "./build/static")  // use the loaded sourouterce
    router.StaticFile("/", "./build/index.html")  // use the loaded source
    
    log.Println("Starting server...")

    router.Run()
}