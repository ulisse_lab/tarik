package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"
)

func InitializeDB(config Config) {
	log.Println("Creating database...")
	database, _ := sql.Open("sqlite3", config.DBPath)
	defer database.Close()
	tx, _ := database.Begin()

	tx.Exec(`CREATE TABLE IF NOT EXISTS filters (id INTEGER PRIMARY KEY AUTOINCREMENT, regex TEXT UNIQUE, results INT, time REAL)`)

	tx.Commit()
}

func ProcessConnections(config Config) {
	log.Println("Matching connections...")
	database, _ := sql.Open("sqlite3", config.DBPath)
	defer database.Close()

	tx, _ := database.Begin()

	tx.Exec(`create table connections as 
	select ID,ip_src,p_src,ip_dest,p_dest,timestamp,content,MIN(connID) AS connID
	from
	(
	SELECT p1.*,P2.ROWID as connID 
		FROM  (traffic  p1 INNER JOIN  (select DISTINCT ip_src,p_src,ip_dest,p_dest    from traffic) P2
		ON (p1.ip_src = p2.ip_dest AND p1.p_src = p2.p_dest AND p1.ip_dest == p2.ip_src and p1.p_dest == p2.p_src   ) )
	union all
	SELECT p1.*,P2.ROWID as connID
		FROM (traffic  p1 INNER JOIN  ( select DISTINCT ip_src,p_src,ip_dest,p_dest    from traffic) P2
	  
		on (P1.ip_src == p2.ip_src AND P1.p_src == p2.p_src  AND p1.ip_dest == p2.ip_dest and p1.p_dest == p2.p_dest))
	)
	GROUP by id
	ORDER BY connID, timestamp;
	`)

	tx.Commit()

	log.Println("Connections loaded!");
}

func ProcessFilter(config Config, regex string) {
	log.Printf("Processing filter: %s", regex)

	database, _ := sql.Open("sqlite3_custom", config.DBPath)
	defer database.Close()
	tx, err := database.Begin()

	res, err := tx.Exec("INSERT OR IGNORE INTO filters (regex, results, time) VALUES (?, ?, ?)",regex, -1, 0)
	fmt.Println(res)
	fmt.Println(err)

	rows, _ := tx.Query(`select id from filters where regex = ?`, regex)

	var filterID = -1;

	for rows.Next() {
		rows.Scan(&filterID)
	}

	if filterID <0 {
		log.Println("Invalid filter entry")
		return
	}

	// Check if the result table already exist
	rows, _ = tx.Query(`SELECT name FROM sqlite_master WHERE type='table' AND name=?;`, fmt.Sprintf("result_%d", filterID))
	if rows.Next() != false {
		log.Println("Result set for filter has already been calculated")
	}else{
		start := time.Now()
		query:= fmt.Sprintf("CREATE TABLE result_%d AS SELECT id, connID FROM connections WHERE regexp(?, content)", filterID)

		tx.Exec(query, regex)

		elapsed := time.Since(start)

		// Count the number of results
		rows, _ := tx.Query(fmt.Sprintf(`select count(*) from result_%d`,filterID))
		var results = 0
		for rows.Next() {
			rows.Scan(&results)
		}

		log.Printf("Results: %d in %s", results, elapsed)

		tx.Exec("UPDATE filters SET results=?, time=? WHERE id=?", results, elapsed.Seconds(), filterID)
	}

	tx.Commit()
}