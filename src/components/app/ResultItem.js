import React from 'react';

export class ResultItem extends React.Component {
    render() {
        let selectedCss = "";
        if (this.props.isSelected) {
            selectedCss = "list-item-selected";
        }

        return (
            <a className={"list-group-item list-group-item-action flex-column align-items-start "+selectedCss}
                     onClick={() => this.props.onItemSelect(this.props.connection)}
               key={this.props.connection.id}
            >
                <div className="d-flex w-100 justify-content-between">
                    <h5 className="mb-1">Connection #{this.props.connection.id}</h5>

                    <small className="text-muted">{this.props.connection.p_src}</small>
                </div>
                <p className="mb-1">S: {this.props.connection.ip_src}:{this.props.connection.p_src} /
                    D: {this.props.connection.ip_dest}:{this.props.connection.p_dest}
                / P: {this.props.connection.packets.length}</p>
            </a>

        );
    }
}