import React from 'react';
import {TextPayloadView} from "./TextPayloadView";
import {HexPayloadView} from "./HexPayloadView";
import {SyntaxPayloadView} from "./SyntaxPayloadView";

function _base64ToArrayBuffer(base64) {
    var binary_string = atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

export class ContentItem extends React.Component {
    render() {
        let cardtype = "card-received";
        if (this.props.source === this.props.packet.src) {
            cardtype = "card-sent"
        }
        if (this.props.isRisky) {
            cardtype += " risky-card"
        }

        let date = new Date(this.props.packet.timestamp);

        let byteArray = _base64ToArrayBuffer(this.props.packet.content);
        let uint8Array = new Uint8Array(byteArray);

        return (
            <div className={"card card-received "+cardtype}>
                <div className="card-body">
                    {this.props.hexView ? (
                        <HexPayloadView content={this.props.packet.content} contentArray={uint8Array} />
                    ) : (
                        this.props.syntaxView !== null ? (
                            <SyntaxPayloadView content={this.props.packet.content} language={this.props.syntaxView} />
                        ) : (
                            <TextPayloadView content={this.props.packet.content} />
                        )
                    )}
                </div>
                <div className="card-footer text-muted">
                    Size: {uint8Array.length} - Timestamp: {date.toUTCString()} - ID: {this.props.packet.id}
                </div>
            </div>
        );
    }
}