import React from 'react';

export class TextPayloadView extends React.Component {
    render() {
        // Divide the payload into multiple lines
        let content = atob(this.props.content).split("\n").map(
            line => <p>{line}</p>
        );

        return (
            <div>
                {content}
            </div>
        );
    }
}