import React from 'react';

export class SearchResult extends React.Component {
    render() {
        return (
            <a className={"list-group-item list-group-item-action flex-column align-items-start "}
               onClick={() => this.props.onFilterSelect(this.props.filter)}
               key={this.props.filter.id}
            >
                <div className="d-flex w-100 justify-content-between">
                    <h5 className="mb-1">{this.props.filter.regex}</h5>
                </div>
                <p className="mb-1">Matching {this.props.filter.results} packets in {this.props.filter.time}s</p>
            </a>
        );
    }
}