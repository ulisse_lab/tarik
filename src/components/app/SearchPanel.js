import React from 'react';
import {ResultItem} from "./ResultItem";
import {SearchResult} from "./SearchResult";
import {ContentItem} from "./ContentItem";
import {withRouter} from "react-router-dom";

class SearchPanel extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            filters: []
        }
    }

    componentDidMount() {
        this.loadFilters();
    }

    loadFilters() {
        fetch(`/filters`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        filters: result
                    });
                },
                (error) => {
                    alert("Connection error!");
                }
            );
    }



    render() {
        return (
            <div>
                <h2 className="mb-3">Results</h2>
                <div className="list-group list-group-hover">
                    {this.state.filters.map(filter => {
                        return  <SearchResult
                            filter={filter}
                            onFilterSelect={(filter) => {
                                this.props.history.push("/navigate");
                                this.props.onFilterSelect(filter)
                            }}
                        />
                    })}
                </div>
            </div>
        );
    }
}

export default withRouter(SearchPanel);